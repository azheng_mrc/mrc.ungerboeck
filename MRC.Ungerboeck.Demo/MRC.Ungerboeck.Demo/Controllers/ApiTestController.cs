﻿using MRC.Ungerboeck.Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UngerboeckSDKPackage;
using UngerboeckSDKWrapper;
using static MRC.Ungerboeck.Demo.UngerboeckApiHelper;
using AutoMapper.QueryableExtensions;

namespace MRC.Ungerboeck.Demo.Controllers
{
    [RoutePrefix("api")]
    public class ApiTestController : ApiController
    {
        private List<EventJobCategoriesModel> eventJobCategories;
        public readonly APIUser apiUser = new APIUser()
        {
            UserID = "UBAPI",
            Password = "ungerboeck",
            UngerboeckUrl = "https://ubtrain.mrc.net.au"
        };

        [HttpGet]
        [Route("account/{orgCode}/{accountCode}")]
        public AllAccountsModel GetAccount(string orgCode, string accountCode)
        {

            var apiClient = new HttpClient();
            try
            {
                return APIUtil.GetAccount(UngerboeckHttpClient(apiUser), orgCode, accountCode);
            }
            catch
            {
                return null;
            }
        }

        [HttpGet]
        [Route("account/{orgCode}/firstName/{firstName}")]
        public List<MembersModel> GetAccountsByFirstName(string orgCode, string firstName)
        {

            var apiClient = new HttpClient();
            try
            {
                var query = $"FirstName eq '{firstName.Replace("'", "''")}'"; 

                var searchMetaDataModel = new SearchMetadataModel();

                var accounts =  APIUtil.GetSearchList<AllAccountsModel>(
                    UngerboeckHttpClient(apiUser),
                    ref searchMetaDataModel,
                    orgCode,
                    query,
                    "",
                    100000,
                    100000,
                    new List<string>()
                    {
                        "FirstName",
                        "LastName"
                    }
                        ).ToList();

                if(accounts?.Count() > 0)
                {
                    return accounts.AsQueryable().ProjectTo<MembersModel>().ToList();

                    //var members = new List<MembersModel>();
                    //foreach(var account in accounts)
                    //{
                    //    members.Add(
                    //        new MembersModel()
                    //        {
                    //            FirstName = account.FirstName,
                    //            LastName = account.LastName,
                    //            MemberNumber = account.AccountCode
                    //        });
                    //}
                    //return members;
                }
            }
            catch
            {
                // eat it
            }
            return null;
        }


        [HttpGet]
        [Route("event/{orgCode}/eventDescription/{eventDescription}")]
        public List<MRCEventModel> getEvents(string orgCode, string eventDescription)
        {

            var apiClient = new HttpClient();
            try
            {
                var query = $"substringof('{eventDescription}', Description)";

                var searchMetaDataModel = new SearchMetadataModel();

                var events = APIUtil.GetSearchList<EventsModel>(
                    UngerboeckHttpClient(apiUser),
                    ref searchMetaDataModel,
                    orgCode,
                    query,
                    "",
                    100000,
                    100000,
                  new List<string>
                  {
                      "EventID",
                      "Description",
                      "StartDate",
                      "StartTime",
                      "Category"
                  }
                        ).ToList();

                if (events?.Count() > 0)
                {
                    var mrcEvents = events.AsQueryable().ProjectTo<MRCEventModel>().ToList();

                    foreach(var evt in events)
                    {
                        var mrcEvent = mrcEvents.Find(f => f.EventId == evt.EventID.Value);

                        if(mrcEvent != null)
                        {
                            mrcEvent.Category = GetEventCategoryDescription(orgCode, evt.Category);
                        }
                    }
                    return mrcEvents;
                    //var members = new List<MembersModel>();
                    //foreach(var account in accounts)
                    //{
                    //    members.Add(
                    //        new MembersModel()
                    //        {
                    //            FirstName = account.FirstName,
                    //            LastName = account.LastName,
                    //            MemberNumber = account.AccountCode
                    //        });
                    //}
                    //return members;
                }
            }
            catch
            {
                // eat it
            }
            return null;
        }

        private string GetEventCategoryDescription(string orgCode, string code)
        {
            try
            {
                if(eventJobCategories == null)
                {
                    var searchMetaDataModel = new SearchMetadataModel();

                    eventJobCategories = APIUtil.GetSearchList<EventJobCategoriesModel>(
                        UngerboeckHttpClient(apiUser),
                        ref searchMetaDataModel,
                        orgCode,
                        "all"
                        ).ToList();
                }

                var eventCategory = eventJobCategories.Find(f => f.Code == code);
                if (eventCategory != null)
                {
                    return eventCategory.Description;
                }
            }
            catch { }
            return string.Empty;
        }

    }
}
