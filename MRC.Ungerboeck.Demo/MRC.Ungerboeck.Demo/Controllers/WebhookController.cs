﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ungerboeck.Integrations.Webhooks;
using Ungerboeck.Integrations.Webhooks.DTOs;
using UngerboeckSDKPackage;
using UngerboeckSDKWrapper;
using static MRC.Ungerboeck.Demo.UngerboeckApiHelper;

namespace MRC.Ungerboeck.Demo.Controllers
{
    [RoutePrefix("webhook")]
    public class WebhookController : ApiController
    {
        public readonly APIUser apiUser = new APIUser()
        {
            UserID = "UBAPI",
            Password = "ungerboeck",
            UngerboeckUrl = "https://ubtrain.mrc.net.au"
        };

        private string ProductServiceCode = "VCHR-MRC-MFD-17082019";

        [HttpPost]
        [Route("AccountUpdated")]
        public GenericResponseDTO AccountOptOutUpdatedAsync(AccountDTO request)
        {
            var response = new GenericResponseDTO(request);

            // get event
            //var @event = APIUtil.GetEvent(UngerboeckApiHelper.UngerboeckHttpClient(apiUser), request.OrgCode, request.Id);


            var change = request.Changes.Find(f => f.PropertyName == "DirectMailOptIn");

            if (change != null)
            {

                if (change.CurrentValue.ToString() == "N" && change.OriginalValue.ToString() == "Y")
                {
                    try
                    {
                        var accountProductService = APIUtil.GetAccountProductService(UngerboeckHttpClient(apiUser), request.OrgCode, request.AccountCode, ProductServiceCode);

                        APIUtil.AwaitDeleteAccountProductService(UngerboeckHttpClient(apiUser), request.OrgCode, request.AccountCode, ProductServiceCode).Wait();
                    }
                    catch (Exception ex)
                    {
                        response.ReturnCode = ReturnCode.Error;

                        response.Message = APIUtil.GetAPIErrorMessage(ex);
                        
                    }
                }
            }
            else
            {
                response.ReturnCode = ReturnCode.Success;
            }



            return response;
      }

        
    }
}
