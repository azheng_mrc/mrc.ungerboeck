﻿using AutoMapper;
using MRC.Ungerboeck.Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UngerboeckSDKPackage;

namespace MRC.Ungerboeck.Demo.App_Start
{
    public static class AutoMapperConfig
    {

        public static void RegisterMappings()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<AllAccountsModel, MembersModel>()
                .ForMember(d => d.FirstName, opt => opt.MapFrom(s => s.FirstName))
                .ForMember(d => d.LastName, opt => opt.MapFrom(s => s.LastName))
                .ForMember(d => d.MemberNumber, opt => opt.MapFrom(s => s.AccountCode))
                .ForMember(d => d.Status, opt => opt.MapFrom(s => s.Region));
            });

            Mapper.Initialize(config =>
            {
                config.CreateMap<EventsModel, MRCEventModel>()
                .ForMember(d => d.EventId, opt => opt.MapFrom(s => s.EventID.Value))
                .ForMember(d => d.EventDescription, opt => opt.MapFrom(s => s.Description))
                //.ForMember(d => d.Category, opt => opt.MapFrom(s => s.Category))
                .ForMember(d => d.EventStartDateTime, opt => opt.MapFrom(s => s.StartDate.Value.Date.Add(s.StartTime.Value.TimeOfDay)))
                .ForMember(d => d.EventEndDateTime, opt => opt.MapFrom(s => s.EndDate.GetValueOrDefault(DateTime.Now).Date.Add(s.EndTime.GetValueOrDefault(DateTime.Now).TimeOfDay)));
            });

        }

    }
}