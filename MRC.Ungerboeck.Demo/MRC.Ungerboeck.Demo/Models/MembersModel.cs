﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRC.Ungerboeck.Demo.Models
{
    public class MembersModel
    {
        public string MemberNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Status { get; set; }

    }
}