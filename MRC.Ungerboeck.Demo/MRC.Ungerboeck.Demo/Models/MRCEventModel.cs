﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRC.Ungerboeck.Demo.Models
{
    public class MRCEventModel
    {
        public int EventId { get; set;}

        public string EventDescription { get; set; }

        public DateTime EventStartDateTime { get; set; }

        public DateTime EventEndDateTime { get; set; }

        public string Category { get; set; }

    }
}