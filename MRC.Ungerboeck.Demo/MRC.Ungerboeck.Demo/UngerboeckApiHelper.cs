﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Caching;
using System.Web;
using UngerboeckSDKWrapper;

namespace MRC.Ungerboeck.Demo
{
    public static class UngerboeckApiHelper
    {
        private static readonly MemoryCache cache = MemoryCache.Default;

        internal static HttpClient UngerboeckHttpClient(APIUser apiUser)
        {

            var cacheKey = JsonConvert.SerializeObject(apiUser);
            var apiClient = cache.Get(cacheKey) as HttpClient;

            if (apiClient == null)
            {
                try
                {
                    apiClient = new HttpClient();
                    APIUtil.RetrieveAPIToken(ref apiClient, apiUser.UserID, apiUser.Password, apiUser.UngerboeckUrl);
                    cache.Add(cacheKey, apiClient, DateTimeOffset.UtcNow.AddMinutes(30));
                }
                catch(Exception ex)
                {
                    ex.ToString();
                    return null;
                }
            }

            return apiClient;

        }




        public class APIUser
        {

            public string UngerboeckUrl { get; set; }

            public string UserID { get; set; }

            public string Password { get; set; }


        }

    }

}